using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using ComplexData.Models;
using HomeWork.Models;
using System.Collections.Generic;
using System;

namespace HomeWork.Controllers
{
    public class DarbuotojasController : Controller
    {
        private ApplicationDbContext _context;

        public DarbuotojasController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Darbuotojas
        /*public IActionResult Index()
        {


        var applicationDbContext = _context.Darbuotojas.Include(d => d.Imone);
            return View(applicationDbContext.ToList());
        }*/

        public IActionResult Index(int ImonesId)   
        {
            var applicationDbContext = _context.Darbuotojas.Include(d => d.Imone);
            var IdQry = from m in _context.Darbuotojas
                        orderby m.ImonesId
                        select m.ImonesId;

            var IdList = new List<int>();
            IdList.AddRange(IdQry.Distinct());
            ViewData["ImonesId"] = new SelectList(IdList);

            var Id = from m in _context.Darbuotojas
                     select m;



            if (ImonesId > 0)
            {
                Id = Id.Where(x => x.ImonesId == ImonesId);
            }

            return View(Id);
        }

        // GET: Darbuotojas/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Darbuotojas darbuotojas = _context.Darbuotojas.Single(m => m.Id == id);
            if (darbuotojas == null)
            {
                return HttpNotFound();
            }

            return View(darbuotojas);
        }

        // GET: Darbuotojas/Create
        public IActionResult Create()
        {
            ViewData["ImonesId"] = new SelectList(_context.Imone, "Id", "Imone");
            return View();
        }

        // POST: Darbuotojas/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Darbuotojas darbuotojas)
        {
            if (ModelState.IsValid)
            {
                _context.Darbuotojas.Add(darbuotojas);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["ImonesId"] = new SelectList(_context.Imone, "Id", "Imone", darbuotojas.ImonesId);
            return View(darbuotojas);
        }

        // GET: Darbuotojas/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Darbuotojas darbuotojas = _context.Darbuotojas.Single(m => m.Id == id);
            if (darbuotojas == null)
            {
                return HttpNotFound();
            }
            ViewData["ImonesId"] = new SelectList(_context.Imone, "Id", "Imone", darbuotojas.ImonesId);

            return View(darbuotojas);
        }

        // POST: Darbuotojas/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Darbuotojas darbuotojas)
        {
            if (ModelState.IsValid)
            {
                _context.Update(darbuotojas);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["ImonesId"] = new SelectList(_context.Imone, "Id", "Imone", darbuotojas.ImonesId);
            return View(darbuotojas);
        }

        // GET: Darbuotojas/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Darbuotojas darbuotojas = _context.Darbuotojas.Single(m => m.Id == id);
            if (darbuotojas == null)
            {
                return HttpNotFound();
            }

            return View(darbuotojas);
        }

        // POST: Darbuotojas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Darbuotojas darbuotojas = _context.Darbuotojas.Single(m => m.Id == id);
            _context.Darbuotojas.Remove(darbuotojas);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }

        
    }
}
