using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using ComplexData.Models;
using HomeWork.Models;

namespace HomeWork.Controllers
{
    public class ImoneController : Controller
    {
        private ApplicationDbContext _context;

        public ImoneController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Imones
        public IActionResult Index()
        {
            return View(_context.Imone.ToList());
        }

        // GET: Imones/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Imone imone = _context.Imone.Single(m => m.Id == id);
            if (imone == null)
            {
                return HttpNotFound();
            }

            return View(imone);
        }

        // GET: Imones/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Imones/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Imone imone)
        {
            if (ModelState.IsValid)
            {
                _context.Imone.Add(imone);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(imone);
        }

        // GET: Imones/Edit/5
        public IActionResult Edit(int? id)
        {
            ViewData["ID"] = id;

            if (id == null)
            {
                return HttpNotFound();
            }

            Imone imone = _context.Imone.Single(m => m.Id == id);
            if (imone == null)
            {
                return HttpNotFound();
            }
            return View(imone);   
        }

        // POST: Imones/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Imone imone)
        {
            if (ModelState.IsValid)
            {
                _context.Update(imone);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(imone);
        }

        // GET: Imones/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Imone imone = _context.Imone.Single(m => m.Id == id);
            if (imone == null)
            {
                return HttpNotFound();
            }

            return View(imone);
        }

        // POST: Imones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Imone imone = _context.Imone.Single(m => m.Id == id);
            _context.Imone.Remove(imone);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }

        
    }
}
