using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using ComplexData.Models;
using HomeWork.Models;

namespace HomeWork.Controllers
{
    public class KompetencijaController : Controller
    {
        private ApplicationDbContext _context;

        public KompetencijaController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Kompetencijas
        public IActionResult Index()
        {
            return View(_context.Kompetencija.ToList());
        }

        // GET: Kompetencijas/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Kompetencija kompetencija = _context.Kompetencija.Single(m => m.Id == id);
            if (kompetencija == null)
            {
                return HttpNotFound();
            }

            return View(kompetencija);
        }

        // GET: Kompetencijas/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Kompetencijas/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Kompetencija kompetencija)
        {
            if (ModelState.IsValid)
            {
                _context.Kompetencija.Add(kompetencija);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kompetencija);
        }

        // GET: Kompetencijas/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Kompetencija kompetencija = _context.Kompetencija.Single(m => m.Id == id);
            if (kompetencija == null)
            {
                return HttpNotFound();
            }
            return View(kompetencija);
        }

        // POST: Kompetencijas/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Kompetencija kompetencija)
        {
            if (ModelState.IsValid)
            {
                _context.Update(kompetencija);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kompetencija);
        }

        // GET: Kompetencijas/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Kompetencija kompetencija = _context.Kompetencija.Single(m => m.Id == id);
            if (kompetencija == null)
            {
                return HttpNotFound();
            }

            return View(kompetencija);
        }

        // POST: Kompetencijas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Kompetencija kompetencija = _context.Kompetencija.Single(m => m.Id == id);
            _context.Kompetencija.Remove(kompetencija);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
