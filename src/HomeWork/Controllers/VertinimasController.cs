using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using ComplexData.Models;
using HomeWork.Models;

namespace HomeWork.Controllers
{
    public class VertinimasController : Controller
    {
        private ApplicationDbContext _context;

        public VertinimasController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Vertinimas
        public IActionResult Index()
        {
            var applicationDbContext = _context.Vertinimas.Include(v => v.Darbuotojas).Include(v => v.Kompetencija);
            return View(applicationDbContext.ToList());
        }

        // GET: Vertinimas/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Vertinimas vertinimas = _context.Vertinimas.Single(m => m.Id == id);
            if (vertinimas == null)
            {
                return HttpNotFound();
            }

            return View(vertinimas);
        }

        // GET: Vertinimas/Create
        public IActionResult Create()
        {
            ViewData["DarbuotojoId"] = new SelectList(_context.Set<Darbuotojas>(), "Id", "Darbuotojas");
            ViewData["KompetencijosId"] = new SelectList(_context.Set<Kompetencija>(), "Id", "Kompetencija");
            return View();
        }

        // POST: Vertinimas/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Vertinimas vertinimas)
        {
            if (ModelState.IsValid)
            {
                _context.Vertinimas.Add(vertinimas);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["DarbuotojoId"] = new SelectList(_context.Set<Darbuotojas>(), "Id", "Darbuotojas", vertinimas.DarbuotojoId);
            ViewData["KompetencijosId"] = new SelectList(_context.Set<Kompetencija>(), "Id", "Kompetencija", vertinimas.KompetencijosId);
            return View(vertinimas);
        }

        // GET: Vertinimas/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Vertinimas vertinimas = _context.Vertinimas.Single(m => m.Id == id);
            if (vertinimas == null)
            {
                return HttpNotFound();
            }
            ViewData["DarbuotojoId"] = new SelectList(_context.Set<Darbuotojas>(), "Id", "Darbuotojas", vertinimas.DarbuotojoId);
            ViewData["KompetencijosId"] = new SelectList(_context.Set<Kompetencija>(), "Id", "Kompetencija", vertinimas.KompetencijosId);
            return View(vertinimas);
        }

        // POST: Vertinimas/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Vertinimas vertinimas)
        {
            if (ModelState.IsValid)
            {
                _context.Update(vertinimas);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["DarbuotojoId"] = new SelectList(_context.Set<Darbuotojas>(), "Id", "Darbuotojas", vertinimas.DarbuotojoId);
            ViewData["KompetencijosId"] = new SelectList(_context.Set<Kompetencija>(), "Id", "Kompetencija", vertinimas.KompetencijosId);
            return View(vertinimas);
        }

        // GET: Vertinimas/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Vertinimas vertinimas = _context.Vertinimas.Single(m => m.Id == id);
            if (vertinimas == null)
            {
                return HttpNotFound();
            }

            return View(vertinimas);
        }

        // POST: Vertinimas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Vertinimas vertinimas = _context.Vertinimas.Single(m => m.Id == id);
            _context.Vertinimas.Remove(vertinimas);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
