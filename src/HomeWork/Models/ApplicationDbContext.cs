﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;
using ComplexData.Models;

namespace HomeWork.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

           
        }
        public DbSet<Imone> Imone { get; set; }
        public DbSet<Vertinimas> Vertinimas { get; set; }
        public DbSet<Kompetencija> Kompetencija { get; set; }
        public DbSet<Darbuotojas> Darbuotojas { get; set; }
    }
}
