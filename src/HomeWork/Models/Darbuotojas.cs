﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace ComplexData.Models
{
    public class Darbuotojas
    {
        public int Id { get; set; }
        public int ImonesId { get; set; }
        public string Vardas { get; set; }
        public string Pavarde { get; set; }
        public string Email { get; set; }
        public int Tel { get; set; }
        public DateTime RegistracijosData { get; set; }

        [ForeignKey("ImonesId")]
        [InverseProperty("Darbuotojai")]
        public virtual Imone Imone { get; set; }
        public virtual ICollection<Vertinimas> Vertinimai { get; set; }
    }
}
