﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ComplexData.Models
{
    public class Imone
    {
        public int Id { get; set; }
        public string Pavadinimas { get; set; }
        public string Aprasymas { get; set; }
        public DateTime SukurimoData { get; set; }

        public virtual ICollection<Darbuotojas> Darbuotojai { get; set; }
    }
}
