﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ComplexData.Models
{
    public class Kompetencija
    {
        public int Id { get; set; }
        public string Pavadinimas { get; set; }
        public string Aprasymas { get; set; }

        public virtual ICollection<Vertinimas> Vertinimai { get; set; }
    }
}
