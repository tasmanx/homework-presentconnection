﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using ComplexData.Models;

namespace HomeWork.Models
{
    public class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<ApplicationDbContext>();

            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }

            if (!context.Imone.Any())
            {
                context.Imone.AddRange(
                 new Imone
                 {
                     Pavadinimas = "Present Connection",
                     Aprasymas = "Olandijos Bendrove isikurusi 2011m.",
                     SukurimoData = DateTime.Parse("2011-01-01")
                 },

                 new Imone
                 {
                     Pavadinimas = "Teo",
                     Aprasymas = "Daugiau info www.teo.lt",
                     SukurimoData = DateTime.Parse("1991-01-01")
                 },

                 new Imone
                 {
                     Pavadinimas = "Bamboo",
                     Aprasymas = "Daugiau info www.bamboo.lt",
                     SukurimoData = DateTime.Parse("2004-01-07")
                 },

                 new Imone
                 {
                     Pavadinimas = "Bite",
                     Aprasymas = "Daugiau info www.bite.lt",
                     SukurimoData = DateTime.Parse("2011-01-01")
                 }
                );
                context.SaveChanges();
            }

            if (!context.Darbuotojas.Any())
            {
                context.Darbuotojas.AddRange(
                new Darbuotojas
                {
                    ImonesId = context.Imone.FirstOrDefault(x => x.Pavadinimas == "Present Connection").Id,
                    Vardas = "Mantas",
                    Pavarde = "Kuncevičius",
                    Email = "m.kuncevicius@gmail.com",
                    Tel = 867000005,
                    RegistracijosData = DateTime.Parse("2016-04-22")
                },

                 new Darbuotojas
                 {
                     ImonesId = context.Imone.FirstOrDefault(x => x.Pavadinimas == "Present Connection").Id,
                     Vardas = "Deividas",
                     Pavarde = "Petraitis",
                     Email = "petraitis@gmail.com",
                     Tel = 867000205,
                     RegistracijosData = DateTime.Parse("2016-04-22")
                 },

                new Darbuotojas
                {
                    ImonesId = context.Imone.FirstOrDefault(x => x.Pavadinimas == "Teo").Id,
                    Vardas = "Deividas",
                    Pavarde = "Petraitis",
                    Email = "petraitis@gmail.com",
                    Tel = 867000205,
                    RegistracijosData = DateTime.Parse("2016-04-22")
                },

                new Darbuotojas
                {
                    ImonesId = context.Imone.FirstOrDefault(x => x.Pavadinimas == "Bite").Id,
                    Vardas = "Jonas",
                    Pavarde = "Jonaitis",
                    Email = "jonaitis@gmail.com",
                    Tel = 867010205,
                    RegistracijosData = DateTime.Parse("2016-04-22")
                },

                new Darbuotojas
                {
                    ImonesId = context.Imone.FirstOrDefault(x => x.Pavadinimas == "Bamboo").Id,
                    Vardas = "Tomas",
                    Pavarde = "Janas",
                    Email = "janas@gmail.com",
                    Tel = 867010205,
                    RegistracijosData = DateTime.Parse("2016-04-22")
                }
                 );
                context.SaveChanges();
            }

            if (!context.Kompetencija.Any())
            {
                context.Kompetencija.AddRange(

                 new Kompetencija
                 {
                     Pavadinimas = "Junior",
                     Aprasymas = "Jaunasis programuotojas"
                 },

                 new Kompetencija
                 {
                     Pavadinimas = "Media",
                     Aprasymas = "Labiau patyręs programuotojas"
                 },

                 new Kompetencija
                 {
                     Pavadinimas = "Senior",
                     Aprasymas = "Vyr programuotojas"
                 },

                 new Kompetencija
                 {
                     Pavadinimas = "IT Architektas",
                     Aprasymas = "Team leadas, turintis daugiau nei 10m patirtį sistemų projektavime ir jų realizavime"
                 },

                 new Kompetencija
                 {
                     Pavadinimas = "PR",
                     Aprasymas = "Viešųjų ryšių specialistas"
                 }
            );

                context.SaveChanges();
            }

            if (!context.Vertinimas.Any())
            {
                context.Vertinimas.AddRange(

                new Vertinimas
                {
                    //DarbuotojoId = context.Darbuotojas.FirstOrDefault(x => x.Pavarde == "Kuncevičius").Id,
                    //KompetencijosId = context.Kompetencija.FirstOrDefault(y => y.Pavadinimas == "Junior").Id,
                    DarbuotojoId = 1,
                    KompetencijosId = 1,
                    Ivertinimas = 10,
                    SukurimoData = DateTime.Parse("2016-04-22")
                }
                /*
                new Vertinimas
                {
                    DarbuotojoId = context.Darbuotojas.FirstOrDefault(x => x.Pavarde == "Petraitis").Id,
                    KompetencijosId = context.Kompetencija.FirstOrDefault(y => y.Pavadinimas == "Media").Id,
                    Ivertinimas = 8,
                    SukurimoData = DateTime.Parse("2016-03-22")
                },

                new Vertinimas
                {
                    DarbuotojoId = context.Darbuotojas.FirstOrDefault(x => x.Pavarde == "Jonaitis").Id,
                    KompetencijosId = context.Kompetencija.FirstOrDefault(y => y.Pavadinimas == "Senior").Id,
                    Ivertinimas = 9,
                    SukurimoData = DateTime.Parse("2016-04-20")
                },

                new Vertinimas
                {
                    DarbuotojoId = context.Darbuotojas.FirstOrDefault(x => x.Pavarde == "Jonaitis").Id,
                    KompetencijosId = context.Kompetencija.FirstOrDefault(y => y.Pavadinimas == "Junior").Id,
                    Ivertinimas = 10,
                    SukurimoData = DateTime.Parse("2015-04-22")
                }*/
            );

                context.SaveChanges();
            }
        }

    }
}
