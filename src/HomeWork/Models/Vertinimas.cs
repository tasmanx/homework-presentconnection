﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ComplexData.Models
{
    public class Vertinimas
    {
        public int Id { get; set; }
        public int DarbuotojoId { get; set; }
        public int KompetencijosId { get; set; }
        public int Ivertinimas { get; set; }
        public DateTime SukurimoData { get; set; }

        [ForeignKey("DarbuotojoId")]
        [InverseProperty("Vertinimai")]
        public virtual Darbuotojas Darbuotojas { get; set; }

        [ForeignKey("KompetencijosId")]
        [InverseProperty("Vertinimai")]
        public virtual Kompetencija Kompetencija { get; set; }
    }
}
